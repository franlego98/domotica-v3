#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_EMBED_TXTFILES := index.html
COMPONENT_EMBED_TXTFILES += jquery.js
COMPONENT_EMBED_TXTFILES += cirrus.css
COMPONENT_EMBED_TXTFILES += main.js
