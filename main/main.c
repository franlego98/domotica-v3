/* Persistent Sockets Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include "driver/gpio.h"
#include "driver/uart.h"
#include "cJSON.h"
#include <esp_http_server.h>

/* An example to demonstrate persistent sockets, with context maintained across
 * multiple requests on that socket.
 * The examples use simple WiFi configuration that you can set via 'make menuconfig'.
 * If you'd rather not, just change the below entries to strings with
 * the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
 */
#define EXAMPLE_WIFI_SSID CONFIG_WIFI_SSID
#define EXAMPLE_WIFI_PASS CONFIG_WIFI_PASSWORD


//#define UART_RELAY

#ifdef UART_RELAY
	char on_seq[] = {0xa0,0x01,0x01,0xa2};
	char off_seq[] = {0xa0,0x01,0x00,0xa1};
#endif


#define GPIO_RELAY

#ifdef GPIO_RELAY
#define GPIO_NUM_JARDIN 4
#define GPIO_NUM_SOTANO 5
#define GPIO_OUTPUT_PIN_SEL ((1ULL << GPIO_NUM_JARDIN) | (1ULL << GPIO_NUM_SOTANO))
#endif

int jardin = 0;
int sotano = 0;


static const char *TAG="DOMOTICA V3";

extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");

extern const uint8_t jquery_js_start[] asm("_binary_jquery_js_start");
extern const uint8_t jquery_js_end[] asm("_binary_jquery_js_end");

extern const uint8_t cirrus_css_start[] asm("_binary_cirrus_css_start");
extern const uint8_t cirrus_css_end[] asm("_binary_cirrus_css_end");

extern const uint8_t main_js_start[] asm("_binary_main_js_start");
extern const uint8_t main_js_end[] asm("_binary_main_js_end");

#define HTTPD_TYPE_JS "application/javascript"
#define HTTPD_TYPE_CSS "text/css"

esp_err_t api_handler(httpd_req_t *req){
	char inbuf[512];
	char sitio[100];
	char accion[100];
	size_t url_len = httpd_req_get_url_query_len(req);
	
	int err = 0;
	
	if(url_len > 0){
		httpd_req_get_url_query_str(req,inbuf,url_len+1);
	
		if( httpd_query_key_value(inbuf,"sitio",sitio,100) 
				== ESP_ERR_NOT_FOUND ||
			httpd_query_key_value(inbuf,"accion",accion,100) 
				== ESP_ERR_NOT_FOUND){
					
			err |= 0x1;
		}
		
		if(strcmp(sitio,"jardin") == 0 && strcmp(accion,"on") == 0){
				jardin = 1;
		}else if(strcmp(sitio,"jardin") == 0 && strcmp(accion,"off") == 0){
				jardin = 0;
		}else if(strcmp(sitio,"sotano") == 0 && strcmp(accion,"on") == 0){
				sotano = 1;
		}else if(strcmp(sitio,"sotano") == 0 && strcmp(accion,"off") == 0){
				sotano = 0;
		}else{
				err |= 0x1;
		}
		
#ifdef GPIO_RELAY
		gpio_set_level(GPIO_NUM_JARDIN,jardin);
		gpio_set_level(GPIO_NUM_SOTANO,sotano);
#endif

#ifdef UART_RELAY
		if(jardin == 1)
			uart_write_bytes(UART_NUM_0,on_seq,4);
		else
			uart_write_bytes(UART_NUM_0,off_seq,4);
#endif
		
		if( httpd_query_key_value(inbuf,"redirect",accion,100) != ESP_ERR_NOT_FOUND ){
			strcpy(accion,"<meta http-equiv=\"Refresh\" content=\"0; url=.\" />");
			httpd_resp_send_chunk(req,accion,strlen(accion));
			httpd_resp_send_chunk(req,NULL,0);
			err|=0x2;
		}
	}
	
	if(err & 0x1){
		httpd_resp_set_status(req,HTTPD_400);
	}
	
	if(!(err & 0x2)){
		cJSON *estado = cJSON_CreateObject();
		cJSON *estado_jardin = cJSON_CreateNumber(jardin);
		cJSON_AddItemToObject(estado,"jardin",estado_jardin);
		cJSON *estado_sotano = cJSON_CreateNumber(sotano);
		cJSON_AddItemToObject(estado,"sotano",estado_sotano);

		char *cadena = cJSON_Print(estado);

		cJSON_Delete(estado);
		httpd_resp_send(req,cadena,strlen(cadena));
	}	

	return ESP_OK;
}


esp_err_t index_handler(httpd_req_t *req)
{
	uint8_t *last = index_html_start;
	for(uint8_t *t = last;t < index_html_end;t++){
		if(strncmp((char*) t,"T0K3N01",7) == 0){
			httpd_resp_send_chunk(req,(char*) last,t-last);
			if(jardin == 1){
				httpd_resp_send_chunk(req,"outline",7);
			}
			t+=7;
			last = t;
		}else if(strncmp((char*) t,"T0K3N02",7) == 0){
			httpd_resp_send_chunk(req,(char*) last,t-last);
			if(jardin == 0){
				httpd_resp_send_chunk(req,"outline",7);
			}
			t+=7;
			last = t;
		}else if(strncmp((char*) t,"T0K3N03",7) == 0){
			httpd_resp_send_chunk(req,(char*) last,t-last);
			if(sotano == 1){
				httpd_resp_send_chunk(req,"outline",7);
			}
			t+=7;
			last = t;
		}else if(strncmp((char*) t,"T0K3N04",7) == 0){
			httpd_resp_send_chunk(req,(char*) last,t-last);
			if(sotano == 0){
				httpd_resp_send_chunk(req,"outline",7);
			}
			t+=7;
			last = t;
		}
		
	}
	httpd_resp_send_chunk(req,(char*) last,index_html_end - last - 1);
	httpd_resp_send_chunk(req,NULL,0);
	
    return ESP_OK;
}

esp_err_t jquery_handler(httpd_req_t *req)
{
	httpd_resp_set_type(req,HTTPD_TYPE_JS);
    httpd_resp_send(req,(const char*) jquery_js_start, jquery_js_end-jquery_js_start-1);
    return ESP_OK;
}

esp_err_t cirrus_handler(httpd_req_t *req)
{
	httpd_resp_set_type(req,HTTPD_TYPE_CSS);
    httpd_resp_send(req,(const char*) cirrus_css_start, cirrus_css_end-cirrus_css_start-1);
    return ESP_OK;
}

esp_err_t main_handler(httpd_req_t *req)
{
	httpd_resp_set_type(req,HTTPD_TYPE_JS);
    httpd_resp_send(req,(const char*) main_js_start, main_js_end-main_js_start-1);
    return ESP_OK;
}


httpd_uri_t index_get = {
    .uri      = "/",
    .method   = HTTP_GET,
    .handler  = index_handler,
    .user_ctx = NULL
};

httpd_uri_t index1_get = {
    .uri      = "/index.html",
    .method   = HTTP_GET,
    .handler  = index_handler,
    .user_ctx = NULL
};

httpd_uri_t jquery_get = {
    .uri      = "/jquery.js",
    .method   = HTTP_GET,
    .handler  = jquery_handler,
    .user_ctx = NULL
};

httpd_uri_t cirrus_get  = {
    .uri      = "/cirrus.css",
    .method   = HTTP_GET,
    .handler  = cirrus_handler,
    .user_ctx = NULL
};

httpd_uri_t main_get  = {
    .uri      = "/main.js",
    .method   = HTTP_GET,
    .handler  = main_handler,
    .user_ctx = NULL
};


httpd_uri_t api_get = {
    .uri      = "/api",
    .method   = HTTP_GET,
    .handler  = api_handler,
    .user_ctx = "probando textooooo"
};

httpd_handle_t start_webserver(void)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    httpd_handle_t server;

    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &index_get);
        httpd_register_uri_handler(server, &index1_get);
        httpd_register_uri_handler(server, &jquery_get);
        httpd_register_uri_handler(server, &cirrus_get);
        httpd_register_uri_handler(server, &api_get);
        httpd_register_uri_handler(server, &main_get);
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    httpd_handle_t *server = (httpd_handle_t *) ctx;
    /* For accessing reason codes in case of disconnection */
    system_event_info_t *info = &event->event_info;

    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        ESP_ERROR_CHECK(esp_wifi_connect());
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP");
        ESP_LOGI(TAG, "Got IP: '%s'",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));

        /* Start the web server */
        if (*server == NULL) {
            *server = start_webserver();
        }
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_DISCONNECTED");
        ESP_LOGE(TAG, "Disconnect reason : %d", info->disconnected.reason);
        if (info->disconnected.reason == WIFI_REASON_BASIC_RATE_NOT_SUPPORT) {
            /*Switch to 802.11 bgn mode */
            esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCAL_11B | WIFI_PROTOCAL_11G | WIFI_PROTOCAL_11N);
        }
        ESP_ERROR_CHECK(esp_wifi_connect());

        /* Stop the webserver */
        if (*server) {
            stop_webserver(*server);
            *server = NULL;
        }
        break;
    default:
        break;
    }
    return ESP_OK;
}

static void initialise_wifi(void *arg)
{
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, arg));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_WIFI_SSID,
            .password = EXAMPLE_WIFI_PASS,
        },
    };
    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
}

void app_main()
{
	
#ifdef GPIO_RELAY
	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE;
	io_conf.mode = GPIO_MODE_OUTPUT;
	io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
	io_conf.pull_down_en = 1;
	io_conf.pull_up_en = 0;
	gpio_config(&io_conf);
	
	gpio_set_level(GPIO_NUM_JARDIN,jardin);
	gpio_set_level(GPIO_NUM_SOTANO,sotano);
#endif

#ifdef UART_RELAY
	uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(UART_NUM_0, &uart_config);
    uart_driver_install(UART_NUM_0, 1024, 0, 0, NULL, 0);

	uart_write_bytes(UART_NUM_0,off_seq,4);
#endif
	
    static httpd_handle_t server = NULL;
    ESP_ERROR_CHECK(nvs_flash_init());
    initialise_wifi(&server);
}
