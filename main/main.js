$(document).ready(function() {

	$("#jardin-on").click( function(e) {
		e.preventDefault();
		$.get("/api?sitio=jardin&accion=on",update_buttons,"json");
	});

	$("#jardin-off").click( function(e) {
		e.preventDefault();
		$.get("/api?sitio=jardin&accion=off",update_buttons,"json");
	});
	
	$("#sotano-on").click( function(e) {
		e.preventDefault();
		$.get("/api?sitio=sotano&accion=on",update_buttons,"json");
	})

	$("#sotano-off").click( function(e) {
		e.preventDefault();
		$.get("/api?sitio=sotano&accion=off",update_buttons,"json");
	});
	
	function update_buttons(data){
		switch (data["jardin"]) {
			case 0:
				$("#jardin-on").removeClass("outline");
				$("#jardin-on").removeAttr("disabled");
				$("#jardin-off").addClass("outline");
				$("#jardin-off").attr("disabled","");
				break;
			case 1:
				$("#jardin-off").removeClass("outline");
				$("#jardin-off").removeAttr("disabled");
				$("#jardin-on").addClass("outline");
				$("#jardin-on").attr("disabled","");
				break;
		}
		
		switch (data["sotano"]) {
			case 0:
				$("#sotano-on").removeClass("outline");
				$("#sotano-on").removeAttr("disabled");
				$("#sotano-off").addClass("outline");
				$("#sotano-off").attr("disabled","");
				break;
			case 1:
				$("#sotano-off").removeClass("outline");
				$("#sotano-off").removeAttr("disabled");
				$("#sotano-on").addClass("outline");
				$("#sotano-on").attr("disabled","");
				break;
		}
	}


	setInterval(function (){
		$.get("/api",update_buttons,"json");
	},5000);
	$.get("/api",update_buttons,"json");
});
