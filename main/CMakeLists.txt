set(COMPONENT_SRCS "main.c")
set(COMPONENT_ADD_INCLUDEDIRS ".")
set(COMPONENT_EMBED_TXTFILES "index.html" "main.js" "cirrus.css" "jquery.js")

register_component()
